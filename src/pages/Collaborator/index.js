import React, { Component } from "react";

import { Form, Row, Col, FloatingLabel} from 'react-bootstrap';

import axios from 'axios';

import Endpoints from 'Endpoints';

import Moldure from 'components/Moldure/index';
import Button from 'components/Button/index';
import List from 'components/List/index';
import CloudImage from 'components/CloudImage/index';

const collaborator = {
  image: '',
  name: '',
  surname: '',
  cpf: '',
  benefit: '',
  dependents: []
};

const errors = {
  image: '',
  name: '',
  surname: '',
  cpf: '',
  benefit: '',
};

const dependent = {
  name: '',
  surname: '',
  cpf: ''
};

const dependentLabels = {
  name: 'Nome',
  surname: 'Sobrenome',
  cpf: 'CPF'
};

class Collaborator extends Component {
  constructor(props) {
    super(props);
    this.state = this.initialState();

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  initialState() {
    return Object.assign(collaborator, {errors: errors})
  }

  setImage(image) {
    this.setState({image: image});
  }

  resetForm() {
    this.setState(this.initialState());
  }

  handleChange(event) {
    let newState = {};
    newState[event.target.name] = event.target.value;
    this.setState(newState);
  }

  setErrors(errors) {
    this.setState(errors);
  }

  handleSubmit(event) {
    axios
    .post(Endpoints.collaborator, {collaborator: this.state})
      .then((res) => {
        alert('Formulário enviado com sucesso: ' + JSON.stringify(this.state));
        this.resetForm();
      })
      .catch((error) => {
        const response = JSON.parse(error.request.response);
        this.setErrors(response);
        console.log(response);
      });
    event.preventDefault();
  }

  render() {
    return (
      <Moldure slot={
        <Form onSubmit={this.handleSubmit}>
          <h2>Preencha seus dados</h2>
          <Row>
            <Col>
              <CloudImage
                fileName={this.state.image}
                setImage={(image) => this.setImage(image)}
              />
            </Col>
            <Col>
              <Form.Group>
                <FloatingLabel
                  label="Nome"
                  className="mb-1"
                >
                  <Form.Control
                    type="text"
                    name="name"
                    value={this.state.name}
                    placeholder="true"
                    onChange={this.handleChange}
                  />
                </FloatingLabel>
                <div style={{fontSize: 12, color: "red"}}>
                  { this.state.errors.name }
                </div>
              </Form.Group>

              <Form.Group>
                <FloatingLabel
                  label="Sobrenome"
                  className="mb-1"
                >
                  <Form.Control
                    type="text"
                    name="surname"
                    value={this.state.surname}
                    placeholder="true"
                    onChange={this.handleChange}
                  />
                </FloatingLabel>
                <div style={{fontSize: 12, color: "red"}}>
                  { this.state.errors.surname }
                </div>
              </Form.Group>

              <Form.Group>
                <FloatingLabel
                  label="CPF"
                  className="mb-1"
                >
                  <Form.Control
                    type="text"
                    name="cpf"
                    value={this.state.cpf}
                    placeholder="true"
                    onChange={this.handleChange}
                  />
                </FloatingLabel>
                <div style={{fontSize: 12, color: "red"}}>
                  { this.state.errors.cpf }
                </div>
              </Form.Group>

              <Form.Select className="mb-1" name="benefit" value={this.state.benefit} onChange={this.handleChange}>
                <option value="" disabled>Selecione o benefício</option>
                <option value="a">Benefício A</option>
                <option value="b">Benefício B</option>
                <option value="c">Benefício C</option>
              </Form.Select>
              <div style={{fontSize: 12, color: "red"}}>
                  { this.state.errors.benefit }
              </div>

              <List
                className="mb-1"
                data={this.state.dependents}
                errors={this.state.errors.dependents}
                struct={dependent}
                structLabels={dependentLabels}
              />
            </Col>
          </Row>
          <Button className="float-end" variant="dark" label="Enviar" type="submit"/>
        </Form>
      }/>
    );
  }
}
 
export default Collaborator;