import React, { Component } from "react";

import Moldure from 'components/Moldure/index';

import SignInUp from 'forms/SignInUp/index';

class Admin extends Component {

  render() {
    return (
      <Moldure
        slot={<SignInUp/>}
      />
    );
  }
}
 
export default Admin;