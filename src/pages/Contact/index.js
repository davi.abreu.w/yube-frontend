import React, { Component } from "react";
 
import Moldure from 'components/Moldure/index';

class Contact extends Component {
  render() {
    return (
      <Moldure
        slot={
          <div>
            <h2>Dúvidas?</h2>
            <p>Visita <a href="https://yube.com.br/">nosso site</a>!
            </p>
          </div>
        }
      />
    );
  }
}
 
export default Contact;