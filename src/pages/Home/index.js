import React, { Component } from "react";

import Moldure from 'components/Moldure/index';

class Home extends Component {
  render() {
    return (
      <Moldure
        slot={
          <div>
            <h2>Olá</h2>
            <p>Aplicação em construção!!!</p>
          </div>
        }
      />
    );
  }
}
 
export default Home;