import React, { Component } from "react";

import { Button as StyledButton } from 'react-bootstrap';

export default class Button extends Component {

  render() {
    return (
      <StyledButton
        className={this.props.className}
        variant={this.props.variant}
        type={this.props.type}
        onClick={this.props.handleClick}
      >
        {this.props.label}
      </StyledButton>
    );
  }

}