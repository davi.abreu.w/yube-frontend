import React, { Component } from "react";

import {v4 as uuidv4} from 'uuid';

import { Card } from 'react-bootstrap';
import {CloudinaryContext as Context, Image} from 'cloudinary-react';

import Button from 'components/Button/index';
import Services from 'Services';

const defaultPublicId = Services.cloudinary.folder + "/sample.png";

<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript" ></script>


export default class CloudImage extends Component {

  constructor(props) {
    super(props);
    this.state = {publicId: this.props.publicId || defaultPublicId};
  }
  
  componentDidMount () {
    const cloudinary = "https://widget.cloudinary.com/v2.0/global/all.js";

    this.loadJavascript(cloudinary);
  }

  loadJavascript(src) {
    const script = document.createElement("script");

    script.src = src;
    script.type = "text/javascript";;
    script.async = true;

    document.body.appendChild(script);
  }

  handleSuccess(error, result) {
    if (!error && result && result.event === "success") { 
      let path = result.info.public_id;
      this.setState({publicId: path});
      this.props.setImage(path);
    }
  }

  openUploadWidget() {
    let publicId = Services.cloudinary.folder + "/" + uuidv4();
    window.cloudinary.openUploadWidget({
        cloudName: Services.cloudinary.cloudname,
        uploadPreset: Services.cloudinary.uploadPreset,
        publicId: publicId
    },
      (error, result) => {
        this.handleSuccess(error, result)
      }
    );
  }

  render() {
    return (
      <Context
        cloudName={Services.cloudinary.cloudname}
        uploadPreset={Services.cloudinary.uploadPreset}
      >
        <Card>
          <Card.Body>
            <Button
                type="button"
                handleClick={this.openUploadWidget.bind(this)}
                label="Carregar imagem"
            />
          </Card.Body>
          <Card.Img
            variant="bottom"
            as={Image}
            publicId={this.state.publicId}
          />
        </Card>
      </Context>
    );
  }

}