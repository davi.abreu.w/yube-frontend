import React, { Component } from "react";

import { Switch, Route } from 'react-router-dom';

export default class Router extends Component {

  render() {
    let routes = this.props.routes;
    return (
      <Switch>
      {
        routes.map(
          (route) => {
            return (
              <Route
                key={route.path}
                exact path={route.path}
              >
                {route.component}
              </Route>
            ) 
          }
        )
      }
      </Switch>
    );
  }

}
