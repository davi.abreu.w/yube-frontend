import React, { Component } from "react";

import { NavLink } from 'react-router-dom';
import { Nav, Navbar } from 'react-bootstrap';

export default class Header extends Component {

  render() {
    const routes = this.props.routes;

    return (
      <Navbar className="navbar-black" bg="dark" variant="dark">
        <Navbar.Brand>
          Yube
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="mr-auto"
            variant="pills"
          >
            {
                routes.map(
                  (route) => {
                    return (
                      <Nav.Link key={route.path} as={NavLink} exact to={route.path}>
                        {route.label}
                      </Nav.Link>
                    )
                  }
                )
              }
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }

}
