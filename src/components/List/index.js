import React, { Component } from "react";

import { Card, Row, Form, FloatingLabel } from 'react-bootstrap';

import Button from 'components/Button/index';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props.data || []
    };
  }

  addItem() {
    let data = this.state.data

    data.push(Object.assign({}, this.props.struct));
    
    this.setState({data: data});
  }

  removeItem(index) {
    let data = this.state.data;
    
    data.splice(index, 1);
    
    this.setState({data: data});
  }

  handleListItemChange(index, item, event) {
    let data = this.state.data;

    item[event.target.name] = event.target.value;
    data[index] = item;
    
    this.setState({data: data});
  }

  key(index, key) {
    return index + " - " + key;
  }

  listItem(item, index) {
    const error = (this.props.errors && this.props.errors[index]) || [];
    return (
      <Card>
        {
          Object.keys(item).map(
            name =>
              <Form key={this.key(index, name)}>
                <Form.Group>
                  <FloatingLabel
                    label={this.props.structLabels[name]}
                    className="mb-1"
                  >
                    <Form.Control
                      type="text"
                      name={name}
                      value={item[name]}
                      placeholder="true"
                      onChange={(event) => this.handleListItemChange(index, item, event)}
                    />
                  </FloatingLabel>
                </Form.Group>
                <div style={{fontSize: 12, color: "red"}}>
                  { error[name] }
                </div>
              </Form>
          )
        }
        {
          <Button
            variant="danger"
            type="button"
            label="Remover"
            handleClick={() => this.removeItem(index)}
          />
        }
      </Card>
    );
  }

  render() {
    return (
      <div>
        <Row>
          <Button
            type="button"
            handleClick={this.addItem.bind(this)}
            label="+ Incluir dependente"
          />
        </Row>
        <Row>{
          this.state.data.map(
            (item, index) =>
              <li key={index}>
                {this.listItem(item, index)}
              </li>
          )
        }</Row>
      </div>
    );
  }
}
 
export default List;