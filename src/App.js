import 'App.css';

import { BrowserRouter } from 'react-router-dom';

import { Container } from 'react-bootstrap';

import Header from 'components/Header/index';
import Router from 'components/Router/index';

import Home from 'pages/Home/index';
import Admin from 'pages/Admin/index';
import Collaborator from 'pages/Collaborator/index';
import Contact from 'pages/Contact/index';

const routes = [
  {
    path: '/',
    label: 'Home',
    component: <Home />
  },
  {
    path: '/colaborador',
    label: 'Cadastrar Requisição',
    component: <Collaborator />
  },
  {
    path: '/admin',
    label: 'Admin',
    component: <Admin />
  },
  {
    path: '/contact',
    label: 'Contato',
    component: <Contact />
  }
];

function App() {
  return (
    <BrowserRouter>
      <Container fluid>
        <Header routes={routes}/>
        <Router routes={routes}/>
      </Container>
    </BrowserRouter>
  );
}

export default App;
