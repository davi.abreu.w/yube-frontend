const backendUrl = process.env.REACT_APP_BACKEND_URL;

const Endpoints = {
  collaborator: `${backendUrl}/api/collaborator/`,
  admin: {
    user: `${backendUrl}/api/admin/user/`,
    login: `${backendUrl}/api/admin/login/`,
    collaboratorReview: (collaboratorId) =>
      {return `${backendUrl}/api/admin/collaborator/${collaboratorId}/review`}
  }
};

export default Endpoints;
