import React, { Component } from "react";

import axios from 'axios';
import Endpoints from 'Endpoints';

import { Form, Row, Col, FloatingLabel} from 'react-bootstrap';
import Button from 'components/Button/index';

const signin = {
  endpoint: Endpoints.admin.login,
  title: "Signin",
  submitLabel: "Signin",
  switchLabel: "Ainda não tem conta?"
}

const signup = {
  endpoint: Endpoints.admin.user,
  title: "Signup",
  submitLabel: "Signup",
  switchLabel: "Já tem conta?"
}

const modeSwitch = {
  [signin.title]: signup,
  [signup.title]: signin,
};

const user = {
  email: '',
  password: ''
};

const errors = {
  login: ''
};

export default class Signup extends Component {

  constructor(props) {
    super(props);
    this.state = this.initialState();

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  initialState() {
    return Object.assign(
      {},
      {
        mode: signin,
        user: user,
        errors: errors
      }
    );
  }
  
  resetForm() {
    this.setState(this.initialState());
  }

  switchMode() {
    let newMode = modeSwitch[this.state.mode.title];
    return this.setState({mode: newMode});
  }

  handleChange(event) {
    let newState = Object.assign({}, this.state.user);
    newState[event.target.name] = event.target.value;
    this.setState({user: newState});
  }

  setErrors(errors) {
    this.setState(errors);
  }

  handleSubmit(event) {
    axios
    .post(this.state.mode.endpoint, this.state.user)
      .then((res) => {
        alert('Formulário enviado com sucesso: ' + JSON.stringify(this.state.user));
        this.resetForm();
      })
      .catch((error) => {
        const response = JSON.parse(error.request.response);
        this.setErrors(response);
        console.log(response);
      });
    event.preventDefault();
  }

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        <h2>{this.state.mode.title}</h2>
        <Row>
          <Col>
            <Form.Group>
              <FloatingLabel
                label="Email"
                className="mb-1"
              >
                <Form.Control
                  type="text"
                  name="email"
                  value={this.state.user.email}
                  placeholder="true"
                  onChange={this.handleChange}
                />
              </FloatingLabel>
              <div style={{fontSize: 12, color: "red"}}>
                { this.state.errors.email }
              </div>
            </Form.Group>

            <Form.Group>
              <FloatingLabel
                label="Password"
                className="mb-1"
              >
                <Form.Control
                  type="password"
                  name="password"
                  value={this.state.user.password}
                  placeholder="true"
                  onChange={this.handleChange}
                />
              </FloatingLabel>
              <div style={{fontSize: 12, color: "red"}}>
                { this.state.errors.password }
              </div>
            </Form.Group>
          </Col>
        </Row>
        <div style={{fontSize: 12, color: "red"}}>
          { this.state.errors.login }
        </div>
        <Button
          variant="secondary"
          label={modeSwitch[this.state.mode.title].switchLabel}
          type="button"
          handleClick={() => {this.switchMode()}}
        />
        <Button
          className="float-end"
          variant="dark"
          label={this.state.mode.submitLabel}
          type="submit"
        />
      </Form>
    );
  }
}
