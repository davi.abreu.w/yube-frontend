# Yube Frontend

## Run locally

* `cp .env.sample .env`
* `yarn install`
* `yarn start`

## Improvement Points

1. ~Add Bootstrap.~
2. Derive ListItem from List.
3. ~Add a READY PLUGIN for images (Cloudinary or Amazon S3).~
4. Enchance Cloudinary Widget configuration.
    * The implementation is fragile:
        * The way the widget is handle has a great overhead opening it.
        * No authentication... public images can be an issue.
        * There are some features not needed as "upload more".
5. List is implemented with form...
    * This is not the better approach because it do not need to be a form to be on the list.
    * Warning in console: form cannot be a child of a form.
6. Tradução das mensagens de erro.

## Decisions

* Encapsulate HTML components like Button/index.js:
    * For code aesthetic purpouse.
        * An ideia here is to find a way to pass props to children (not a problem for now)

## Strategy:
  
  1. Simple SPA
  2. Basic Form function (this was friday 04/06/2021)
  3. Setup online and integrate prototypes.
  4. All Form fields set. (this was monday 07/06/2021)
  5. Add image plugin:
      * First option: Cloudinary :checked: :
          * I already did a simple setup with cloudinary... in a CMS project using Strapi:
          * The free plan can handle production demand for a long time depending on the requisites.
      * Second option: AWS S3.
          * I already used a bucket from S3 and did maintenance in Import/Export code (Elixir)
          * But I never setup one.
          * If I can: not to waste my free credits... I prefer for now.
          * I need to study AWS facilities :+1: (it's on my map).
  6. Add Bootstrap:
      * Our first usability/perfumery achievment <3
  7. Checkpoint:
      * APP 1 (Designed and Running)
      * First planned for 07/06/2021 night (23:59)

### Progress:
  1. Checkpoint 4 (07/06/2021 15:30)
  2. Reading Cloudinary documentation [4] (07/06/2021 15:30)
  3. Implemented Cloudinary plugin (07/06/2021 21:00)
  4. First bootstrap component (08/06/2021 12:55)
  5. Finished bootstrap (08/06/2021 15:40)
  6. Add form validation (08/06/2021 17:00)


## References

  * React Events:
      * https://pt-br.reactjs.org/docs/handling-events.html [1]
  * React Lists:
      * https://gitlab.com/davi.abreu.w/react-contacts-davi [2]
  * React Cloud Images:
      * https://github.com/cloudinary/cloudinary_js [3]
      * https://github.com/cloudinary/cloudinary-react [4]
      * https://cloudinary.github.io/cloudinary-react/docs/?path=/docs/example-cloudinarycontext--basic [7]
      * https://cloudinary.com/documentation/react_integration#3_set_cloudinary_configuration_parameters [8]
  * Pure Javascript CMS - graphql free from effort api using strapi scaffolding:
      * https://strapi.io/ [5]
      * https://gitlab.com/davi.abreu.w/ragna-memory-cms [6] study case - site for game Ragnarok Online content.
  * Bootstrap:
      * https://react-bootstrap.github.io/components/
  * Form validation:
      * https://www.youtube.com/watch?v=FM2RN8rHCTE&ab_channel=BenAwad

## Dificuldades encontradas

  * Levei duas horas para desenvolver essa lista... apesar de ter me inspirado na minha lista anterior...
  * Cloudinary documentação/componentização pobre... gastei um tempo até entender como criar o Widget de upload... e está funcionando 'precariamente'.
  * Tive um pouco de dificuldades com a parte do bootstrap... CSS/estilo é de longe uma das minhas habilidades mais fracas.
      * Navlink dando refresh na página: usar Nav.Link as={Navlink}
      * Navlink selecionando sempre 'home' e mais algum: exact path no Nav.Link
      * CSS do bootstrap redefinindo o App.ccs: ordenação dos imports no index.js
  * Bootstrap: Form.Control.Feedback não funcionou direito